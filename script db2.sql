use studentaccounting;

create table Groupp (
	groupName varchar(16) not null,
	faculty varchar(45),
	primary key (groupName)
) ENGINE=INNODB;

create table Exam_Module (
	control varchar(10) not null,
    primary key (control)
) ENGINE=INNODB;
    
insert into Exam_Module values ('Exam');
insert into Exam_Module values ('Module');
    
create table Discipline (
	discId int not null auto_increment,
    discName varchar(45),
    control varchar(10),
    foreign key (control) references Exam_Module(control),
    primary key (discId)
) ENGINE=INNODB;
    


create table Student (
	studentId int not null AUTO_INCREMENT,
	name varchar(45),
	surname varchar(45),
	fatherName varchar(45),
	photo varchar(100),
	entryYear int2,
	birthYear int2,
	address varchar(95),
    groupName varchar(16),
	primary key (studentId),
	foreign key (groupName) references Groupp(groupName)
) ENGINE=INNODB;
    
create table Scholar (
	studentId int not null,
    stipend decimal,
    foreign key (studentId) references Student(studentId),
    primary key (studentId)
) ENGINE=INNODB;

create table Rating (
	studentId int not null,
    semester int1,
    rating decimal,
    foreign key (studentId) references Student(studentId),
    primary key (studentId)
) ENGINE=INNODB;

create table Student_Discipline (
	studentId int not null,
    discId int not null,
    mod1Result int,
    mod2Result int,
    semesterPoint100 int,
    semesterPoint5 int,
    semesterNo int,
    lecturer varchar(45),
    foreign key (studentId) references Student(studentId),
    foreign key (discId) references Discipline(discId),
    primary key (studentId, discId)
) ENGINE=INNODB;

INSERT INTO `studentaccounting`.`groupp` (`groupName`,`faculty`) VALUES (Java-22, EPAM);
INSERT INTO `studentaccounting`.`groupp` (`groupName`, `faculty`) VALUES ('Java-21', 'ЕРАТ');
INSERT INTO `studentaccounting`.`groupp` (`groupName`, `faculty`) VALUES ('CS', 'IKNI');
INSERT INTO `studentaccounting`.`groupp` (`groupName`, `faculty`) VALUES ('AT-42', 'EMAP');
INSERT INTO `studentaccounting`.`groupp` (`groupName`, `faculty`) VALUES ('BI', 'ЄПАМ');

INSERT INTO `studentaccounting`.`student` (`name`, `surname`, `fatherName`, `photo`, `entryYear`, `birthYear`, `address`, `groupName`) VALUES ('Назар', 'Сабара', 'Батькович', 'сихів', '2014', '1997', 'сихів', 'Java-22')
INSERT INTO `studentaccounting`.`student` (`name`, `surname`, `fatherName`, `photo`, `entryYear`, `birthYear`, `address`, `groupName`) VALUES ('Артем', 'Кострич', 'null', 'гурт', '2015', '1998', 'не сихів', 'Java-22')	
INSERT INTO `studentaccounting`.`student` (`name`, `surname`, `fatherName`, `photo`, `entryYear`, `birthYear`, `address`, `groupName`) VALUES ('Ростислав', 'Пікулик', 'нулл', 'ввв', '2013', '1996', 'банк', 'Java-22')	
INSERT INTO `studentaccounting`.`student` (`name`, `surname`, `fatherName`, `photo`, `entryYear`, `birthYear`, `address`, `groupName`) VALUES ('Василь', 'Безпрізвищний', 'Азіатович', 'тіпа шлях до файлу', '2015', '1996', 'Земля', 'CS');
INSERT INTO `studentaccounting`.`student` (`name`, `surname`, `fatherName`, `photo`, `entryYear`, `birthYear`, `address`, `groupName`) VALUES ('Іван', 'Проданець', 'Іванович', '♥', '2015', '1994', 'Нове давидково', 'CS');

INSERT INTO `studentaccounting`.`discipline` (`discName`, `control`) VALUES ('Java 1', 'Module');
INSERT INTO `studentaccounting`.`discipline` (`discName`, `control`) VALUES ('OOP', 'Exam');

INSERT INTO `studentaccounting`.`student_discipline` (`studentId`, `discId`, `mod1Result`, `mod2Result`, `semesterPoint100`, `semesterPoint5`, `semesterNo`, `lecturer`) VALUES ('4', '1', '50', '23', '73', '4', '1', 'Pavelchak');
INSERT INTO `studentaccounting`.`student_discipline` (`studentId`, `discId`, `mod1Result`, `mod2Result`, `semesterPoint100`, `semesterPoint5`, `semesterNo`, `lecturer`) VALUES ('5', '2', '34', '53', '53', '3', '1', 'Dziuba');
INSERT INTO `studentaccounting`.`student_discipline` (`studentId`, `discId`, `mod1Result`, `mod2Result`, `semesterPoint100`, `semesterPoint5`, `semesterNo`, `lecturer`) VALUES ('6', '1', '43', '56', '64', '4', '1', 'Dziuba');
INSERT INTO `studentaccounting`.`student_discipline` (`studentId`, `discId`, `mod1Result`, `mod2Result`, `semesterPoint100`, `semesterPoint5`, `semesterNo`, `lecturer`) VALUES ('7', '2', '52', '34', '54', '3', '2', 'Dodo');
INSERT INTO `studentaccounting`.`student_discipline` (`studentId`, `discId`, `mod1Result`, `mod2Result`, `semesterPoint100`, `semesterPoint5`, `semesterNo`, `lecturer`) VALUES ('8', '1', '53', '52', '65', '4', '2', 'Dutka');

INSERT INTO `studentaccounting`.`scholar` (`studentId`, `stipend`) VALUES ('4', '1300');
INSERT INTO `studentaccounting`.`scholar` (`studentId`, `stipend`) VALUES ('5', '1455');
INSERT INTO `studentaccounting`.`scholar` (`studentId`, `stipend`) VALUES ('6', '1464');
INSERT INTO `studentaccounting`.`scholar` (`studentId`, `stipend`) VALUES ('7', '4623');
INSERT INTO `studentaccounting`.`scholar` (`studentId`, `stipend`) VALUES ('8', '6412');

INSERT INTO `studentaccounting`.`rating` (`studentId`, `semester`) VALUES ('4', '1');
INSERT INTO `studentaccounting`.`rating` (`studentId`, `semester`) VALUES ('5', '1');
INSERT INTO `studentaccounting`.`rating` (`studentId`, `semester`) VALUES ('6', '1');
INSERT INTO `studentaccounting`.`rating` (`studentId`, `semester`, `rating`) VALUES ('8', '1', '100');
INSERT INTO `studentaccounting`.`rating` (`studentId`, `semester`) VALUES ('7', '1');
