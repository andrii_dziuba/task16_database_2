use labor_sql;

#task 4.1  (A, B, E expected and got)
select distinct maker from product where model in (select model from pc);

#task 4.2  (A, B, E expected and got)
select distinct maker from product where product.type = all (select type from product where product.type='PC');

#task 4.3  (A, B, E expected and got)
select distinct maker from product  where product.type = any (select type from product where model=1121);

#task 4.4  (A, B expected and got)
select distinct maker from product where product.type='PC' and maker in (select maker from product where product.type='laptop');

#task 4.5  (A, B expected and got)
select distinct maker from product where maker <> all (select maker from (select distinctrow maker, type from product where product.type='PC' or product.type='laptop') as tab1 group by maker having count(maker)=1) and product.type<>'printer';

#task 4.6  (A, B expected and got)
select distinct maker from product where product.type='laptop' and maker=any(select maker from product where product.type='PC');

#task 4.7  (A, B, E expected and got)
select distinct maker from product where model in (select model from pc);

#task 4.8
select country, class from classes where country='Ukraine' or class in (select distinct class from ships);

#task 4.9 (California Surigao Strait 1944-10-25 expected and got)
select ship, battle, date from outcomes inner join battles on outcomes.battle=battles.name where result='OK' and ship in (select ship from outcomes where result='damaged');

#task 4.10 (7 expected and got)
select count(model) from pc where model in (select model from product where maker='A');

#task 4.11 (E expected and got)
select distinct maker from product where model <> all (select model from pc) and product.type='PC';

#task 4.12 (ids: 3, 4, 5 expected and got)
select * from laptop where price > all (select price from pc);

#task 5.1 (E expected and got)
select distinct maker from product where not exists (select * from pc where model = product.model) and product.type='PC';

#task 5.2 (B, A expected and got)
select maker from product where exists (select * from pc where model=product.model and speed>=750) and product.type='PC';

#task 5.3 (A, B as expected)
select distinct maker from product where exists (select * from (select model from pc where speed>=750 union select model from laptop where speed>=750) as p where product.model=p.model);

#task 5.4 ??
select maker, speed from pc inner join product on product.model=pc.model where maker in
(select distinct maker from product where product.type='printer' and exists (select maker from product where product.type='PC'));

#task 5.5 (As expected)
select name, launched, displacement from ships inner join classes on ships.class=classes.class where exists (select * from ships as s where launched>=1922 and displacement>=35000 and s.name=ships.name);

#task 5.6 (as expected)
select distinct class from ships where exists (select * from outcomes where result='sunk' and ship=ships.name);

#task 5.7 (as expected)
select distinct maker from product where product.type='laptop' and exists (select * from product as p where product.maker=p.maker and p.type='Printer');

#task 5.8 (as expected)
select distinct maker from product where product.type='laptop' and not exists (select * from product as p where product.maker=p.maker and p.type='Printer');

#task 6.1 (as required)
select concat('average price - ', convert(avg(price), char)) from laptop;

#task 6.2 (as expected)
select concat('code: ', code) as field1, concat('model: ', model) as field2, 
concat('speed: ', speed) as field3, concat('ram: ', ram) as field4, 
concat('hd: ', hd) as field5, concat('cd: ', cd) as field6, concat('price: ', price) as field7 from pc;

#task 6.3 (as expected)
select concat(year(date), '.', month(date), '.', day(date)) as date from income;

#task 6.4 (as expected)
select ship, battle, case result when 'OK' then 'добре' when 'damaged' then 'пошкоджено' when 'sunk' then 'затоплено' end as result from outcomes;

#task 6.5 (as expected)
select trip_no, date, ID_psg, substring(place, 1, 1) as Ряд, substring(place, 2, 1) as Місце from pass_in_trip;

#task 6.6 (as expected)
select trip_no, ID_comp, plane, concat('from ', town_from, ' to ', town_to) as from_to, time_out, time_in from trip;

#task 7.1 (as expected)
select model, price from printer where price=(select max(price) from printer);

#task 7.2 (as expected)
select type, p.model, speed from laptop l inner join product p on l.model=p.model where speed < all (select speed from pc);

#task 7.3 (as expected)
select maker, price from printer inner join product on product.model=printer.model where color like 'y' and price=(select min(price) from printer where color like 'y');

#task 7.4 (as expected)
select maker, count(maker) from product where type like 'PC' group by maker having count(maker)>1;

#task 7.5 (as expected)
select avg(price) from pc inner join product on product.model=pc.model where maker in (select maker from product where type like 'pr%');

#task 7.6 (as expected)
select date, count(date) from pass_in_trip inner join trip on pass_in_trip.trip_no=trip.trip_no where town_from='london' group by date;

#task 7.7 (as expected)
select date, count(date) from (select date, count(date) from pass_in_trip inner join trip on pass_in_trip.trip_no=trip.trip_no where town_to='moscow' group by date) as p;

#task 7.8 ??
select * from (select country, launched, count(launched) from classes inner join ships on classes.class=ships.class group by launched, country order by launched) as p;

#task 7.9 (as expected)
select * from ships inner join outcomes on outcomes.ship=ships.name inner join classes on classes.class=ships.class group by country having count(battle)>1 ;

#task 8.1 (as expected)
select maker, (select count(code) from pc where pc.model=product.model) as pc, (select count(code) from laptop where laptop.model=product.model) as laptop, (select count(code) from printer where printer.model=product.model) as printer from product;

#task 8.2
select maker, avg(screen) from laptop inner join product on laptop.model=product.model group by maker;

#task 8.3
select maker, (select max(price) from pc inner join product on pc.model=product.model where product.maker=p.maker) as max_price from product p where type like 'pc';

#task 8.4
select maker, (select min(price) from pc inner join product on pc.model=product.model where product.maker=p.maker) as min_price from product p where type like 'pc';

#task 8.5
select speed, (select avg(price) from pc where speed=p.speed) as avg_price from pc p where speed>600;

#task 8.6
select maker, (select avg(hd) from pc inner join product pp on pp.model=pc.model where pp.maker=product.maker) as avg_hd from pc p inner join product on product.model=p.model where maker in (select maker from product where type like 'prin%') group by maker;

#task 8.7
select ship, (select displacement from classes where classes.class=(select class from ships where ships.name=outcomes.ship)) as displacement, (select numGuns from classes where classes.class=(select class from ships where ships.name=outcomes.ship)) as numGuns from outcomes where battle like 'gua%';

#task 8.8
select ship, (select country from classes where classes.class=(select class from ships where ships.name=outcomes.ship)) as displacement, (select numGuns from classes where classes.class=(select class from ships where ships.name=outcomes.ship)) as numGuns from outcomes where result like 'da%';

#task 9.1
select maker, 
case 
when (select count(code) from pc inner join product p on pc.model=p.model where p.maker=product.maker) = 0 then 'no'
when (select count(code) from pc inner join product p on pc.model=p.model where p.maker=product.maker) <> 0 then concat('yes(', (select count(code) from pc inner join product p on pc.model=p.model where p.maker=product.maker), ')')
end as tt
from product group by maker;

#task 9.2
select outcome_o.point, outcome_o.date, outcome_o.out, income_o.inc from outcome_o join income_o on income_o.date=outcome_o.date;

#task 9.3
select * from ships inner join classes on classes.class=ships.class
where case when numGuns=8 then 1 else 0 end +
case when bore=15 then 1 else 0 end +
case when displacement=32000 then 1 else 0 end +
case when type='bb' then 1 else 0 end +
case when country='usa' then 1 else 0 end +
case when launched=1915 then 1 else 0 end +
case when ships.class='kon' then 1 else 0 end >= 4;

#task 9.4
select *,
(case when timediff(time_in, time_out) < 0 then timediff(time_out, time_in) else timediff(time_in, time_out) end) as time
from trip;

#task 9.5 ??
select * from outcome left join outcome_o on outcome_o.point=outcome.point
union
select * from outcome left join outcome_o on outcome_o.point=outcome.point
group by outcome.date, outcome_o.date;

#task 10.1
select maker, product.model, type, price from product inner join laptop on laptop.model=product.model where maker like 'b' and type like 'laptop'
union
select maker, product.model, type, price from product inner join pc on pc.model=product.model where maker like 'b' and type like 'pc';

#task 10.2
select product.type, product.model, max(price) from product inner join laptop on laptop.model=product.model where product.type like 'laptop' group by model
union
select product.type, product.model, max(price) from product inner join pc on pc.model=product.model where product.type like 'pc' group by model
union
select product.type, product.model, max(price) from product inner join printer on printer.model=product.model where product.type like 'pri%' group by model;

#task 10.3
select avg(price) from 
(select price from product inner join laptop on laptop.model=product.model where maker like 'a' and type like 'laptop'
union
select price from product inner join pc on pc.model=product.model where maker like 'a' and type like 'pc') as t1;

#task 10.4
select name from ships
union 
select ship from outcomes;

#task 10.5
select distinct * from
(select name from (select *, count(class) c1 from ships group by class) as bbb where c1=1
union
select name from (select *, count(class) c2 from outcomes inner join ships on ships.name=outcomes.ship group by class) as ccc where c2=1) as xxx;

#task 10.6 
select class, count(class) c1 from ships group by class
union
select class, count(class) c2 from (select ship from outcomes where outcomes.ship not in (select name from ships)) as o inner join ships on ships.name=o.ship group by class;

#task 10.7
select name from ships where launched<1942
union
select ship from (select ship from outcomes where outcomes.ship not in (select name from ships)) as o inner join ships on ships.name=o.ship where launched<1942;
